class AppRoute {
  // Auth
  static const String login = '/login';
  static const String forgetPassword = '/forgetPassword';
  static const String verifyCodeForgetPassword = '/verifyCodeForgetPassword';
  static const String resetPassword = '/resetPassword';
  static const String successResetPassword = '/successResetPassword';

  static const String onBoarding = '/onBoarding';
  static const String language = '/language';

  static const String deliveryHomeScreen = '/deliveryHomeScreen';
  static const String deliveryTracking = '/deliveryTracking';
  static const String deliveryOrdersDetails = '/deliveryOrdersDetails';
}
