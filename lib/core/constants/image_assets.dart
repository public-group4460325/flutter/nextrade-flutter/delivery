class AppImageAsset {
  static const String rootImage = 'assets/images';
  // Auth
  static const String logo = '$rootImage/auth/Logo.png';
  static const String forgetPassword = '$rootImage/auth/forget_password.png';
  static const String resetPassword = '$rootImage/auth/reset_password.png';
  static const String verifyCode = '$rootImage/auth/verify_code.png';
}
