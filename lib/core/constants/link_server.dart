class AppLinkServer {
  // Server
  static const String server = "http://10.0.2.2:8000";
  static const String auth = "$server/api/auth";
  static const String users = "$server/api/users";
  static const String admins = "$server/api/admins";
  static const String deliveries = "$server/api/deliveries";
  static const String storage = "$server/storage";

  // ================================= Auth ==================================
  static const String login = '$auth/login';
  static const String logout = '$auth/logout';
  static const String resendVerificationCode = '$auth/resendVerificationCode';

  // Forget Password
  static const String resetPassword = '$auth/resetPassword';
  static const String checkEmail = '$auth/checkEmail';
  static const String verifyCodeForgetPassword =
      '$auth/verifyCodeForgetPassword';
  // ========================================================================

  // ============================== Deliveries ==============================
  //Orders
  static const String deliveryApproveOrder = '$deliveries/orders/approve';
  static const String deliver = '$deliveries/orders/deliver';
  static const String getPendingOrdersDelivery =
      '$deliveries/orders/getPending';
  static const String getArchivedOrdersDelivery =
      '$deliveries/orders/getArchived';
  static const String getAcceptedOrdersDelivery =
      '$deliveries/orders/getAccepted';
  static const String getDetailsOrderDelivery = '$deliveries/orders/getDetails';
  // ========================================================================
}
