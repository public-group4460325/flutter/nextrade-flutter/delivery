import 'package:flutter/material.dart';

class AppColor {
  // Light
  static const Color grey = Color(0xFF8e8e8e);
  static const Color black = Colors.black;
  static const Color primaryColor = Color(0xFF3498db);
  static const Color secondaryColor = Color(0xFF2980b9);
  static const Color backGroundColor = Color(0xFFFFFFFF);
  static const Color textFormColor = Color.fromARGB(22, 34, 149, 243);
  static const Color notificationButtonColor = Colors.blueGrey;

  //Dark
  // static const Color grey = Color(0xFF8e8e8e);
  // static const Color black = Colors.black;
  // static const Color primaryColor = Color(0xFF001f3f);
  // static const Color secondaryColor = Color(0xFFe5c100);
  // static const Color backGroundColor = Color(0xFF001f3f);
  // static const Color textFormColor = Color.fromARGB(22, 34, 149, 243);
  // static const Color notificationButtonColor = Colors.blueGrey;
}
