import 'dart:convert';
import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';

import '../functions/check_internet.dart';
import 'status_request.dart';

class CRUD {
  Future<Either<StatusRequest, Map>> postData({
    required String linkUrl,
    Map<String, String>? headers,
    Map? body,
  }) async {
    try {
      headers ??= {};
      body ??= {};
      if (await CheckInternet()) {
        var response =
            await http.post(Uri.parse(linkUrl), headers: headers, body: body);
        if (response.statusCode == 200 || response.statusCode == 201) {
          if (kDebugMode) {
            print(response.body);
          }
          Map responseBody = jsonDecode(response.body);
          if (responseBody.containsKey('data')) {
            if (responseBody['data'].isEmpty) {
              return const Left(StatusRequest.noData);
            } else {
              return Right(responseBody);
            }
          } else {
            return Right(responseBody);
          }
        } else {
          return const Left(StatusRequest.serverFailed);
        }
      } else {
        return const Left(StatusRequest.offLineFailed);
      }
    } catch (e) {
      return const Left(StatusRequest.serverException);
    }
  }

  Future<Either<StatusRequest, Map>> addRequestWithImageOne(
      url, data, File? image, Map<String, String> headers,
      [String? namerequest]) async {
    if (namerequest == null) {
      namerequest = "files";
    }

    var uri = Uri.parse(url);
    var request = http.MultipartRequest("POST", uri);
    request.headers.addAll(headers);

    if (image != null) {
      var length = await image.length();
      var stream = http.ByteStream(image.openRead());
      stream.cast();
      var multipartFile = http.MultipartFile(namerequest, stream, length,
          filename: basename(image.path));
      request.files.add(multipartFile);
    }

    // add Data to request
    data.forEach((key, value) {
      request.fields[key] = value;
    });
    // add Data to request
    // Send Request
    var myrequest = await request.send();
    // For get Response Body
    var response = await http.Response.fromStream(myrequest);
    if (response.statusCode == 200 || response.statusCode == 201) {
      if (kDebugMode) {
        print(response.body);
      }
      Map responsebody = jsonDecode(response.body);
      return Right(responsebody);
    } else {
      return const Left(StatusRequest.failed);
    }
  }
}
