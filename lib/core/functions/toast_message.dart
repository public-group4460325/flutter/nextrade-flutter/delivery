import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:nextrade/core/constants/color.dart';

toastMessage({required String message}) {
  Fluttertoast.showToast(
    msg: message,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
    backgroundColor: Colors.grey[300],
    textColor: AppColor.primaryColor,
    timeInSecForIosWeb: 1,
    fontSize: 15,
  );
}
