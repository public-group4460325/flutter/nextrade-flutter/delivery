import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:nextrade/core/constants/color.dart';

uploadImageCamera() async {
  final XFile? file = await ImagePicker()
      .pickImage(source: ImageSource.camera, imageQuality: 90);
  if (file != null) return File(file.path);
  null;
}

uploadImageGallery({isSvg = false}) async {
  FilePickerResult? result = await FilePicker.platform.pickFiles(
    type: FileType.custom,
    allowedExtensions:
        isSvg ? ['svg', 'SVG'] : ['png', 'PNG', 'jpg', 'JPG', 'gif'],
  );
  if (result != null) return File(result.files.single.path!);
  null;
}

showBottomMenu(Function() uploadImageCamera, Function() uploadImageGallery) {
  Get.bottomSheet(
    Container(
      padding: const EdgeInsets.all(10),
      height: 200,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'choose_image'.tr,
            style: const TextStyle(
              fontSize: 22,
              color: AppColor.primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
          const Padding(padding: EdgeInsets.only(top: 10)),
          ListTile(
            onTap: () {
              uploadImageCamera();
              Get.back();
            },
            leading: const Icon(Icons.camera, size: 40),
            title: Text('image_from_camera'.tr,
                style: const TextStyle(fontSize: 20)),
          ),
          ListTile(
            onTap: () {
              uploadImageGallery();
              Get.back();
            },
            leading: const Icon(Icons.image, size: 40),
            title: Text('image_from_gallery'.tr,
                style: const TextStyle(fontSize: 20)),
          )
        ],
      ),
    ),
    backgroundColor: Colors.white,
  );
}
