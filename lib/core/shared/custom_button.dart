import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final void Function() onPressed;
  const CustomButton({
    super.key,
    required this.text,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: AppDimensions.height / 50),
      child: MaterialButton(
        padding: EdgeInsets.symmetric(vertical: AppDimensions.height / 60),
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            color: AppColor.primaryColor,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        textColor: AppColor.primaryColor,
        color: AppColor.backGroundColor,
        onPressed: onPressed,
        child: Text(text),
      ),
    );
  }
}
