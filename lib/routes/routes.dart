import 'routes_links.dart';

List<GetPage<dynamic>> routes = [
  GetPage(
    name: AppRoute.deliveryOrdersDetails,
    page: () => const OrderDetails(),
    binding: OrderDetailsBindings(),
  ),
  GetPage(
    name: AppRoute.deliveryTracking,
    page: () => const Tracking(),
    binding: TrackingBindings(),
  ),
  GetPage(
    name: AppRoute.deliveryHomeScreen,
    page: () => const HomeScreen(),
    binding: HomeScreenBindings(),
  ),
  GetPage(
    name: '/',
    page: () => const Login(),
    binding: LoginBindings(),
    middlewares: [MyMiddleware()],
  ),
  GetPage(
    name: AppRoute.forgetPassword,
    page: () => const ForgetPassword(),
    binding: ForgetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.verifyCodeForgetPassword,
    page: () => const VerifyCodeForgetPassword(),
    binding: VerifyCodeForgetPasswordBindings(),
  ),GetPage(
    name: AppRoute.successResetPassword,
    page: () => const SuccessResetPassword(),
    binding: SuccessResetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.resetPassword,
    page: () => const ResetPassword(),
    binding: ResetPasswordBindings(),
  ),
];
