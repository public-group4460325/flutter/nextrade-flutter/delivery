library routes_links;

export 'package:get/get.dart';
export 'package:nextrade/controllers/auth/login_controller/bindings.dart';
export 'package:nextrade/controllers/home_screen_controller/bindings.dart';
export 'package:nextrade/controllers/orders/order_details_controller/bindings.dart';
export 'package:nextrade/controllers/tracking_controller/bindings.dart';
export 'package:nextrade/core/constants/routes.dart';
export 'package:nextrade/core/middlewares/my_middlelware.dart';
export 'package:nextrade/view/screens/auth/login.dart';
export 'package:nextrade/view/screens/home_screen.dart';
export 'package:nextrade/view/screens/orders/order_details.dart';
export 'package:nextrade/view/screens/tracking.dart';
export 'package:nextrade/controllers/auth/forget_password/forget_password_controller/bindings.dart';
export 'package:nextrade/controllers/auth/forget_password/reset_password_controller/bindings.dart';
export 'package:nextrade/controllers/auth/forget_password/success_reset_password_controller/bindings.dart';
export 'package:nextrade/controllers/auth/forget_password/verify_code_forget_password_controller/bindings.dart';
export 'package:nextrade/view/screens/auth/forget_password/forget_password.dart';
export 'package:nextrade/view/screens/auth/forget_password/reset_password.dart';
export 'package:nextrade/view/screens/auth/forget_password/success_reset_password.dart';
export 'package:nextrade/view/screens/auth/forget_password/verify_code_forget_password.dart';

