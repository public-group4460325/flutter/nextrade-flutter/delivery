import 'package:get/get.dart';

import 'controller.dart';

class SuccessResetPasswordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SuccessResetPasswordControllerImp>(
        () => SuccessResetPasswordControllerImp());
  }
}
