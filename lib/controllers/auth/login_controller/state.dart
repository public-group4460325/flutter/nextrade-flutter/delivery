import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/classes/status_request.dart';
import '../../../core/services/my_services.dart';
import '../../../data/data_source/remote/auth/login_data.dart';

class LoginState {
  late TextEditingController emailController;
  late TextEditingController passwordController;
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  LoginData loginData = LoginData(Get.find());
  StatusRequest statusRequest = StatusRequest.none;
  bool ishHidden = true;
  MyServices myServices = Get.find();
}
