import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/archived_orders_controller/controller.dart';

class ArchivedOrdersBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ArchivedOrdersControllerImp>(
        () => ArchivedOrdersControllerImp());
  }
}
