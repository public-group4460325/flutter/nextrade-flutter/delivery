import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/accepted_orders_controller/controller.dart';

class AcceptedOrdersBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AcceptedOrdersControllerImp>(() => AcceptedOrdersControllerImp ());
  }
}
