import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/pending_orders_controller/controller.dart';

class PendingOrdersBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PendingOrdersControllerImp>(() => PendingOrdersControllerImp ());
  }
}
