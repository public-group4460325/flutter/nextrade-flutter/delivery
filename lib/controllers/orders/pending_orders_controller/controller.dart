import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/accepted_orders_controller/controller.dart';
import 'package:nextrade/controllers/orders/pending_orders_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/order_model.dart';

abstract class PendingOrdersController extends GetxController {
  initData();
  getOrders();
  String getPaymentType(String val);
  toDeliveryOrderDetailsPage(Order order);
  approveOrder(int orderId);
  refreshPage();
  // getCurrentLocation();
}

class PendingOrdersControllerImp extends PendingOrdersController {
  final state = PendingOrdersState();

  @override
  void onInit() {
    initData();
    getOrders();
    super.onInit();
  }

  @override
  void initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    super.onInit();
  }

  @override
  getOrders() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response =
          await state.ordersData.getPendingOrdersDelivery(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.orders.addAll(response['data'].map((e) => Order.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  String getPaymentType(val) {
    if (val == 'cash') return 'cash_on_delivery'.tr;
    if (val == 'cards') return 'payment_cards'.tr;
    return '';
  }

  @override
  toDeliveryOrderDetailsPage(order) {
    Get.toNamed(AppRoute.deliveryOrdersDetails, arguments: {'order': order});
  }

  @override
  approveOrder(orderId) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response =
          await state.ordersData.deliveryApproveOrder(orderId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        // getCurrentLocation();
        AcceptedOrdersControllerImp controller = Get.find();
        controller.state.orders
            .add(state.orders.firstWhere((element) => element.id == orderId));
        state.orders.removeWhere((element) => element.id == orderId);
        if (state.orders.isEmpty) state.statusRequest = StatusRequest.noData;
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  refreshPage() {
    getOrders();
  }

  // @override
  // getCurrentLocation() {
  //   state.positionStream = Geolocator.getPositionStream(locationSettings: null)
  //       .listen((Position? position) {
  //     print('===================');
  //     print(position!.latitude);
  //     print(position.longitude);
  //   });
  // }
}
