import 'package:get/get.dart';
import 'package:nextrade/controllers/home_screen_controller/controller.dart';
import 'package:nextrade/controllers/orders/accepted_orders_controller/controller.dart';
import 'package:nextrade/controllers/orders/archived_orders_controller/controller.dart';
import 'package:nextrade/controllers/orders/pending_orders_controller/controller.dart';
class HomeScreenBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeScreenControllerImp>(
        () => HomeScreenControllerImp());
    Get.put(PendingOrdersControllerImp());
    Get.put(AcceptedOrdersControllerImp());
    Get.put(ArchivedOrdersControllerImp());
  }
}
