import 'package:get/get.dart';
import 'package:nextrade/controllers/home_screen_controller/state.dart';

abstract class  HomeScreenController extends GetxController {
  changePage(int currentPage);
}

class  HomeScreenControllerImp extends  HomeScreenController {
  final state =  HomeScreenState();

  @override
  changePage(currentPage) {
    state.currentPage = currentPage;
    update();
  }
}
