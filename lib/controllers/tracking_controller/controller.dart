import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/controllers/tracking_controller/state.dart';
import 'package:nextrade/controllers/orders/archived_orders_controller/controller.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/get_decode_polyline.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';

abstract class TrackingController extends GetxController {
  initData();
  getCurrentLocation();
  initPolyline();
  refreshLocation();
  deliverOrder();
}

class TrackingControllerImp extends TrackingController {
  final state = TrackingState();

  @override
  void onInit() {
    initData();
    initPolyline();
    refreshLocation();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.statusRequest = StatusRequest.none;
    state.address = Get.arguments['address'];
    state.order = Get.arguments['order'];
    state.id = state.myServices.sharedPreferences.getString('id')!;
  }

  @override
  getCurrentLocation() {
    state.kGooglePlex = CameraPosition(
      target: LatLng(state.address.locationLat!, state.address.locationLong!),
      zoom: 12,
    );
    state.markers.add(
      Marker(
        markerId: const MarkerId('current'),
        position:
            LatLng(state.address.locationLat!, state.address.locationLong!),
      ),
    );
    state.positionStream =
        Geolocator.getPositionStream(locationSettings: null).listen(
      (Position? position) {
        state.currentLat = position!.latitude;
        state.currentLong = position.longitude;
        if (state.controller != null) {
          state.controller!.animateCamera(
            CameraUpdate.newLatLng(
              LatLng(
                position.latitude,
                position.longitude,
              ),
            ),
          );
        }
        state.markers
            .removeWhere((element) => element.mapsId.value == 'current');
        state.markers.add(
          Marker(
            markerId: const MarkerId('dest'),
            position: LatLng(position.latitude, position.longitude),
          ),
        );
        update();
      },
    );
  }

  @override
  initPolyline() async {
    Future.delayed(const Duration(seconds: 1));
    state.polylineSet = await getDecodePolyline(
      state.currentLat,
      state.currentLong,
      state.address.locationLat,
      state.address.locationLong,
    );
    update();
  }

  @override
  refreshLocation() async {
    await Future.delayed(const Duration(seconds: 2));
    state.timer = Timer.periodic(const Duration(seconds: 10), (timer) {
      FirebaseFirestore.instance
          .collection('delivery')
          .doc('${state.order.id}')
          .set({
        'lat': state.currentLat,
        'long': state.currentLong,
        'delivery_id': state.id,
      });
    });
  }

  @override
  deliverOrder() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response =
          await state.ordersData.deliver(state.order.id!, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        ArchivedOrdersControllerImp controller = Get.find();
        controller.state.orders.add(state.order);
        Get.offAllNamed(AppRoute.deliveryHomeScreen);
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  void onClose() {
    state.positionStream!.cancel();
    state.controller!.dispose();
    state.timer!.cancel();
  }
}
