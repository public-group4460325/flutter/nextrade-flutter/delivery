import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/orders_data.dart';
import 'package:nextrade/data/models/address_model.dart';
import 'package:nextrade/data/models/order_model.dart';

class TrackingState {
  late StatusRequest statusRequest;
  MyServices myServices = Get.find();
  late String id;
  late GoogleMapController? controller;
  List<Marker> markers = [];
  CameraPosition kGooglePlex =
      const CameraPosition(target: LatLng(0, 0), zoom: 14.4746);
  double? currentLat;
  double? currentLong;
  late Address address;
  late Order order;
  StreamSubscription<Position>? positionStream;
  Set<Polyline> polylineSet = {};
  Timer? timer;
  OrdersData ordersData = OrdersData(Get.find());
  late String apiToken;
}
