import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomNavBarButton extends StatelessWidget {
  final IconData icon;
  final void Function() onPressed;
  final String title;
  final bool isActive;
  const CustomNavBarButton({
    super.key,
    required this.icon,
    required this.onPressed,
    required this.title,
    required this.isActive,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            icon,
            color: isActive ? AppColor.primaryColor : AppColor.black,
          ),
          Text(
            title,
            style: TextStyle(
              color: isActive ? AppColor.primaryColor : AppColor.black,
              fontSize: 10,
            ),
          ),
        ],
      ),
    );
  }
}
