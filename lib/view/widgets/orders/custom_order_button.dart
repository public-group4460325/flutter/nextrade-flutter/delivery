import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';


class CustomOrderButton extends StatelessWidget {
  final void Function() onPressed;
  final String text;
  const CustomOrderButton({
    super.key,
    required this.onPressed,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(
        side: const BorderSide(
          color: AppColor.primaryColor,
          width: 3,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
      textColor: AppColor.primaryColor,
      color: Colors.white,
      onPressed: onPressed,
      child: Text(text),
    );
  }
}
