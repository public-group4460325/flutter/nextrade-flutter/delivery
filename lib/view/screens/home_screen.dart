import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/home_screen_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/functions/exit_alert_app.dart';
import 'package:nextrade/view/widgets/delivery_home_screen/custom_nav_bar.dart';

class HomeScreen extends GetView<HomeScreenControllerImp> {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ExitAlertApp,
      child: GetBuilder<HomeScreenControllerImp>(
        builder: (controller) => Scaffold(
          appBar: AppBar(
            title: Text(controller.state
                .bottomNavBarActions[controller.state.currentPage]['title']),
          ),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          backgroundColor: AppColor.backGroundColor,
          bottomNavigationBar: const CustomNavBar(),
          body: controller.state.pagesList
              .elementAt(controller.state.currentPage),
        ),
      ),
    );
  }
}
