import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class OrdersData {
  CRUD crud;
  OrdersData(this.crud);
  getPendingOrdersDelivery(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getPendingOrdersDelivery,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getArchivedOrdersDelivery(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getArchivedOrdersDelivery,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getAcceptedOrdersDelivery(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getAcceptedOrdersDelivery,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getDetailsOrderDelivery(int orderId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getDetailsOrderDelivery,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'order_id': '$orderId'},
    );
    return response.fold((l) => l, (r) => r);
  }

  deliveryApproveOrder(int orderId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.deliveryApproveOrder,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'order_id': '$orderId'},
    );
    return response.fold((l) => l, (r) => r);
  }

  deliver(int orderId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.deliver,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'order_id': '$orderId'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
