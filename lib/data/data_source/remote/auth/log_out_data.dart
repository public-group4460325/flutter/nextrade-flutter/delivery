import '../../../../core/classes/crud.dart';
import '../../../../core/constants/link_server.dart';

class LogOutData {
  CRUD crud;
  LogOutData(this.crud);
  postData(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.login,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
