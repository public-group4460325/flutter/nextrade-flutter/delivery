import '../../../../../core/classes/crud.dart';
import '../../../../../core/constants/link_server.dart';

class CheckEmailData {
  CRUD crud;
  CheckEmailData(this.crud);
  postData({
    required String email,
  }) async {
    var body = {
      'email': email,
    };
    var response =
        await crud.postData(linkUrl: AppLinkServer.checkEmail, body: body);
    return response.fold((l) => l, (r) => r);
  }
}
